/*
** ETNA PROJECT, 04/04/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include "../../include/my_list.h"
#include <stdlib.h>

int my_strcmp(const char *s1, const char *s2);

char *deleteNode(linked_list_t **list, char **tab)
{
    linked_list_t *temp = *list;
    linked_list_t *prev = NULL;
    int cmp = 1 ;
    char *x ;
    
    if(*list == NULL){
        return "-1";
    }

    while (temp != NULL)
        {
            cmp = my_strcmp(temp->key,tab[0]);
            if (cmp == 0){
                if (prev == NULL){
                    *list = temp->next;
                    x =  temp->data;
                } else {
                    prev->next = temp->next;
                    x =  temp->data;
                }
            }
            temp = temp->next;
        }
    return x;
}
