##
## ETNA PROJECT, 21/03/2020 by bellou_a
## [...]
## File description:
##      [...]
##
CC = gcc
CFLAGS= -Wall -Wextra -Werror -std=c11
EXEC = my_crd
NAME = libmy.a
SRC =   lib/my/my_putchar.c \
		lib/my/my_readline.c \
        lib/my/my_putstr.c  \
		lib/my/my_strlen.c  \
        lib/my/my_getnbr.c  \
        lib/my/my_putnbr.c  \
        lib/my/my_isneg.c   \
        lib/my/my_swap.c    \
        lib/my/my_strcpy.c  \
        lib/my/my_strncpy.c \
        lib/my/my_strcmp.c  \
        lib/my/my_strncmp.c \
        lib/my/my_strcat.c  \
        lib/my/my_strncat.c \
        lib/my/my_strstr.c  \
        lib/my/my_strdup.c \
		lib/my/my_push_front_to_list.c \
		lib/my/my_str_to_word_array.c \
		lib/my/my_count_words.c \
		lib/my/my_cmp_list.c\
		lib/my/my_update.c  \
		lib/my/my_find_func.c\
		lib/my/my_delete_Nodes.c\
		lib/my/my_get_params.c\

OBJ = $(SRC:%.c=%.o)
LIB = ar r $(NAME) $(OBJ)
RAN = ranlib $(NAME)
RM = rm -f
CP = cp $(NAME) ./lib/
$(NAME):	$(OBJ)
			$(LIB)
			$(RAN)
			$(CP)
			$(RM) $(NAME)
			$(CC) $(CFLAGS) main.c -o $(EXEC) -I -L lib/$(NAME)
all:		$(NAME) 

delete:
			$(RM) ./lib/$(NAME)
clean:		delete
			$(RM) $(OBJ) $(EXEC)
fclean:		clean
			$(RM) $(NAME)
re: fclean all
