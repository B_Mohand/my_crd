/*
** ETNA PROJECT, 18/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

int my_strlen(const char *str)
{                                                                                                                                                                           int len = 0;
    while (str[len] != '\0')
        len++;
    return len;
}

char *my_strstr(char *str, const char *to_find)
{
    int i = 0;
    int j = 0;
    int lenFind = my_strlen(to_find);
    for(i=0;  i<=my_strlen(str); i++){
        for(j=0; j<lenFind; j++)
            if(str[i+j]!=to_find[j])
                break;
        if(j==lenFind)
            return str+i;
    }
    return 0;
}

