/*
** ETNA PROJECT, 18/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

int my_strlen(const char *str)
{
    int len = 0;
    while (str[len] != '\0')
        len++;
    return len;
}

char *my_strncat(char *dest, const char *src, int nb)
{
    int i = 0;
    int lendest=my_strlen(dest);
    while( i <= nb && src[i] != '\0' ){
        dest[lendest+i]=src[i];
        i++;
    }    
    return dest;
}
