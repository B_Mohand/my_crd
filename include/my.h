/*
** ETNA PROJECT, 21/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#ifndef MY_H
#define  MY_H
#include "my_list.h"

char *my_readline(void);
void my_putchar(char c);
void my_putstr(const char *str);
int my_strlen(const char *str);
int my_getnbr(const char *str);
void my_putnbr(int nb);
void my_isneg(int nb);
void my_swap(int *a, int *b);
char *my_strcpy(char *dest, const char *src);
char *my_strncpy(char *dest, const char *src, int n);
int my_strcmp(const char *s1, const char *s2);
int my_strncmp(const char *s1, const char *s2, int n);
char *my_strcat(char *dest, const char *src);
char *my_strncat(char *dest, const char *src, int nb);
char *my_strstr(char *str, const char *to_find);
char *my_strdup(const char *src);
void *my_update(linked_list_t *list,char **tab);
linked_list_t *my_push_front_to_list(linked_list_t *list,char **tab);
char **my_get_params (char *c);
char *my_find_func(linked_list_t *list,char **tab);
int my_count_words(char *x);
int my_cmp_list(linked_list_t *list,char **tab);
char *deleteNode(linked_list_t **list, char **tab);

#endif
