/*
** ETNA PROJECT, 31/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include "../../include/my_list.h"
#include <stdlib.h>

int my_strcmp(const char *s1, const char *s2);

char *my_find_func(linked_list_t *list,char **tab)
{
    char *find ;
    int cmp = 0;
    while(list != NULL){
        cmp = my_strcmp(list->key,tab[0]);
        if (cmp == 0)
            {
                find = list->data;
                break;                                                                                                                                                               }                                                                                                                                                                   list = list->next;
    }
    if ( list == NULL || cmp != 0){
        find = "-1";
    }
    return find;
}
