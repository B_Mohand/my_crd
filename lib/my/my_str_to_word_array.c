/*
** ETNA PROJECT, 18/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include <stdlib.h>

int count_word(const char *str)
{
    int i = 0;
    int word = 0;
    int len  = 0;
    while(str[i] != '\0'){
        word = 0;
        while((str[i] == ' ' || str[i] == '\t')  && str[i] != '\0')
           i++;  
        while(( str[i] >= 'a' && str[i] <='z' ) || ( str[i] >= 'A' && str[i] <='Z' ) || ( str[i] >= '0' && str[i] <='9' )){
           i++;
           word++;
        }
         if(str[i] == '\0')
             break;
         if(word !=0)
         len++; 
         while(str[i] != ' ' && str[i] != '\0' && str[i] != '\t'){
             if(( str[i] >= 'a' && str[i] <='z' ) || ( str[i] >= 'A' && str[i] <='Z' ) || ( str[i] >= '0' && str[i] <='9' ))
                 break;
            i++;
         }
    }
    return len;
}

char **my_str_to_word_array(const char *str)
{
    int i = 0;
    int c = 0;
    int w = 0;
    int start = 0;
    int end = 0;
    char **tab;
    int len = count_word(str);
    tab = malloc(sizeof(char*) *(len+1));
    while(str[i] != '\0'){
        c = 0;
        while((str[i] == ' ' || str[i] == '\t')  && str[i] != '\0')
            i++;
        start = i;
        while(( str[i] >= 'a' && str[i] <='z' ) || ( str[i] >= 'A' && str[i] <='Z' ) || ( str[i] >= '0' && str[i] <='9' ))
            i++;
        end = i;
        tab[w] = malloc(sizeof(char)* (end-start+1));
        if(str[i] == '\0')
            break;
        while(start != end){
            tab[w][c] = str[start];
            start++;
            c++;
        }
        tab[w][c]='\0';
        while(str[i] != ' ' && str[i] != '\0' && str[i] != '\t'){
            if(( str[i] >= 'a' && str[i] <='z' ) || ( str[i] >= 'A' && str[i] <='Z' ) || ( str[i] >= '0' && str[i] <='9' ))
                break;
            i++;
        }
        w++;
    }
    tab[len]=NULL;
    return tab;
}
