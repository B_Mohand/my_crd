/*
** ETNA PROJECT, 24/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include "../../include/my_list.h"
#include <stdlib.h>

linked_list_t *my_push_front_to_list(linked_list_t *list,char **tab)
{
    linked_list_t *node;
    node=malloc(sizeof(*node));
    if(node == NULL)
        return 0;
    node->next = list;
    node->key = tab[0];
    node->data = tab[1];
    return node;
}
