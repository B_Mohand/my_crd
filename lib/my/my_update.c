/*
** ETNA PROJECT, 31/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include "../../include/my_list.h"
#include <stdlib.h>

int my_strcmp(const char *s1, const char *s2);

void my_update(linked_list_t *list,char **tab)
{
    linked_list_t *node;
    int cmp;
    node = list;
    while(node != NULL){
        cmp = my_strcmp(node->key,tab[0]);
        if (cmp == 0)
            {
                node->data = tab[1];
                break;
            }
        node = node->next;
    }
}
