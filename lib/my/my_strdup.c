/*
** ETNA PROJECT, 18/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include <stdlib.h>




int my_strlen(const char *str)
{
    int i = 0;
    while (str[i] != '\0'){
        i++;
    }
    return i;
}



char *my_strcpy(char *dest, const char *src)
{
    int i = 0;
    while(src[i] != '\0'){
        dest[i] = src[i];
        i++;
    }
    dest[i] = '\0';
    return dest;
}


char *my_strdup(const char *src){
    char *text;
    int len = my_strlen(src);
    text = (char*)malloc((len+1)*sizeof(char));
    my_strcpy(text,src);
    return 0;
}
