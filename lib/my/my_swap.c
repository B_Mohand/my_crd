/*
** ETNA PROJECT, 16/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/


void my_putchar(char c);

void my_swap(int *a , int *b)
{
    int x = 0;
    x = *a;
    *a = *b;
    *b = x;
}
