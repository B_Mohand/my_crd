/*
** ETNA PROJECT, 17/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/


char *my_strncpy(char *dest, const char *src,int n)
{
    int i = 0;
    while(i <= n && src[i] != '\0'){
        dest[i] = src[i];
        i++;
    }
    dest[i] = '\0';
    return dest;
}

