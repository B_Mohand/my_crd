/*
** ETNA PROJECT, 31/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/
int my_count_words(char *x)
{
    int i = 0;
    int len  = 0;
    while(x[i] != '\0'){ 
        while ((x[i] == ' ' || x[i] == '\t')  && x[i] != '\0')
            i++;
        if(x[i] == '\0')
            break;
        len++;
        while(x[i] != ' ' && x[i] != '\0' && x[i] != '\t')
            i++;
    }
    return len;
}
