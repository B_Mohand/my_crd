/*
** ETNA PROJECT, 31/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include "include/my.h"
#include "include/my_list.h"

int main()
{    
    char *x;
    int cmp_act;
    int exit = 1;
    char **tab;
    int cmp = 6;
    linked_list_t *list;
    char *fnd;
    list = NULL;
    
    while(exit != 0){
        x =  my_readline();
        tab = my_get_params(x);
        if (tab[1] == NULL){
            fnd = my_find_func(list,tab);
            my_putstr(fnd);
            my_putstr("\n");
        }
        if (tab[1] != NULL){
            cmp_act=my_strcmp(tab[1],"D");
            if (cmp_act != 0){
                if (list == NULL){
                    if (my_strcmp(tab[1],"-1") != 0){
                        list=my_push_front_to_list(list,tab);
                        my_putstr(tab[0]);
                        my_putstr("\n");
                    }
                    else { my_putstr(tab[1]);
                        my_putstr("\n");
                    }
                }
                else  {
                    cmp = my_cmp_list(list,tab);
                    if ( cmp != 0){
                        list=my_push_front_to_list(list,tab);
                        my_putstr(tab[0]);
                        my_putstr("\n");
                    }
                    else {
                        my_update(list,tab);
                        my_putstr(tab[0]);
                        my_putstr("\n");
                    }
                }
            } else {
                char *z = deleteNode(&list,tab);
                my_putstr(z);
                my_putstr("\n");
            }
        }
        exit = my_strcmp(x,"exit");
    }
}

