/*
** ETNA PROJECT, 25/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/
#ifndef MY_LIST_H
#define MY_LIST_H


typedef struct linked_list_s
{
    char *key;
    char *data;
    struct linked_list_s *next;
}linked_list_t;

#endif
