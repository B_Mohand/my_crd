/*
** ETNA PROJECT, 31/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

#include <stdlib.h>

int my_count_words(char *x);

char **my_get_params (char *x)
{
    int i = 0;
    int j = 0;
    int k = 0;
    int c = 0;
    int ok = 0;
    char **tab;
    int len = 0;
    len = my_count_words(x);
    tab = malloc(sizeof(char*) *(len+1));
    if (len > 2){
        tab[0] = "-1";                                                                                                                                                          tab[1] = "-1";
        return tab;
    }
    else
        while (i < len){
            while(x[j] != '\0' && ( x[j] == ' ' || x[j] == '\t') )
                j++;
            k = j;
            while(x[k] != '\0' && x[k] != ' ' && x[j]!= '\t')
                {
                    if (x[k]== '\0' || x[k] =='\t')
                        break;
                    if(i == 0)
                        {
                            if(x[k] >= '0' && x[k] <= '9'){
                                k++;
                                ok = 1;
                            }
                            else { ok = 2;
                                break;
                            }
                        }
                    if (i == 1)
                        {
                            if(x[k]>='0' && x[k] <= '9'){
                                k++;
                                ok = 1;
                            }
                            else{ if ( x[k] == 'D' && (x[k+1] == '\0' || x[k+1] == ' ' || x[k+1] == '\t')){
                                    k++;
                                    ok = 1;}
                                else {ok = 2;
                                    break;
                                }
                            }
                        }
                    
                }
            if (ok==1)
                {
                    tab[i] = malloc(sizeof(char)* (k-j+1));
                    while(j <  k){
                        tab[i][c] = x[j];                                                                                                                                                       j++;
                        c++;
                    }
                    tab[i][c] = '\0';
                    c = 0;
                }
            if(ok==2)
                tab[i] = "-1";
            ok = 0;
            i++;
        }
    tab[len] = NULL ;
    return tab;
}
