/*
** ETNA PROJECT, 17/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

int my_strlen(const char *str)
{
    int i = 0;
    while (str[i] != '\0') {
        i++;
    }
    return i;
}

char *my_strcpy(char *dest, const char *src)
{
    int i = my_strlen(src)-1;
    int j = 0;
    while(j <= i){
        dest[j] = src[j];
        j++;
    }
    dest[j] = '\0';
    return dest;
}
